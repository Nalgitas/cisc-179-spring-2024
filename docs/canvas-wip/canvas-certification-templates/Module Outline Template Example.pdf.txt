# Module Outline Template Example
Course Title: Online Faculty Certification Course: Accessibility in Online Learning
Course Description (Directed towards your students):
Online Faculty Certification Course: Accessibility in Online Learning is a self-paced asynchronous course.
The main purpose of this course is to provide you with the best practices in creating an inclusive and
accessible course for you and your students. We will first touch on the basic concepts of accessibility and
how it plays an important role in online course design, guidelines, and legal requirements. Then a series
of activities where you will get to explore how to adopt proper accessibility compliance in your own
online classes. Now that online learning has become a crucial part of our education system. We all need
to do our part in making learning accessible and inclusive to all students in each of our respective fields. I
look forward to learning more about everyone in this class and seeing how far we can go to push online
learning to be the best it can possibly be.
Course Objective:
1.
2.
3.
4.
5.
Create a fully accessible video with proper usage of closed captioning.
Describe core tenants of Title 5 and Section 508.
Knowing when to declare an image as decorative or apply alternative text.
Applying an appropriate usage of external and internal hyperlinks.
Develop skills and understanding of when to make DSPS accessibility accommodation
for a student.
6. Formulate documents and PDF’s and follow proper accessibility standards such as tags,
headings, and text formatting.
7. Show proper usage of the built in Canvas accessibility checker along with the Pope Tech
accessibility tool.
8. Become familiar with applying all course objectives to your Canvas courses.Module 0: Orientation
•
•
•
•
•
•
•
•
0.0
0.1
0.2
0.3
0.4
0.5
0.6
0.7
Module 1 Overview (Reading)
Canvas Face to Face (Reading)
Instructional Content (Reading)
Hands-on: Edit Profile (Graded Assignment)
Hands-on: Add Announcement
(Graded Assignment)
Unit 1 Quiz
(Graded Quiz)
First Impressions
(Graded Discussion)
Module 0 Summary (Reading)
Module 1 Outline: Module 1: Introduction
•
•
•
•
•
•
•
•
•
•
•
•
•
•
•
•
•
1.0
1.1
1.2
1.3
1.4
1.5
1.6
1.7
1.8
1.9
1.10
1.11
1.12
1.13
1.14
1.15
1.16
Module 1 Overview - Start Here!
(Reading)
About Our Class
(Reading)
About Canvas (Reading)
About Your Instructor (Reading)
Introductory Discussion: About You (Graded Discussion)
Required Materials (Reading)
Participation & "Attendance"
(Reading)
Discussion Guidelines (Reading)
Grading & Late Submissions (Reading)
Finding Grades & Feedback (Reading)
Academic Integrity (Reading)
Student Support Resources (Reading)
Technical Support
(Reading)
Mobile App for Canvas
(Reading)
Student Readiness Tutorials (Reading)
Self-Check Quiz: Module 1 (Graded Quiz)
Module 1 Summary (Reading)
Module 2 Outline: Module 2: Accessibility and what is it?
•
•
•
•
•
•
•
•
2.0
2.1
2.2
2.3
2.4
2.5
2.6
2.7
Module 2 Overview (Reading)
Let’s learn about accessibility for the blind (Reading)
Accessibility tools in use
(Reading)
Canvas Accessibility Checkers (Reading)
Discussion: Module 2 – What is accessibility to you?
(Graded Discussion)
Self-Check Quiz: Module 2 (Graded Quiz)
Assignment: Learning Accessibility Guidelines
(Graded Assignment)
Module 2 Summary (Reading)