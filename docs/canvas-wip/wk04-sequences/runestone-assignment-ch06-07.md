# 4.2 Runestone Ch 6 and 7

This week you're going to practice iterating (using a `for` loop) through sequences (`list`s) of objects.

Chapter 6 will show you how to create sequences (`list`s and `tuple`s). And you will practice retrieving any element that you need from that `list` using square brackets around an index integer such as 0, 1, 2 or even -1 (`any_list[0]`). Python even lets you slice up lists into smaller lists using the colon character between the start and stop of the slice (`any_list[1:3]`).

In Chapter 7 you will finally learn how to iterate through a sequence and do things many times without having to copy and paste lines of code over and over. This is a key skill for helping you automate some of the drudgery in your life. A Python program will happily iterate through a sequence of things to do as many times as you like.

Head over to Runestone, read Chapters 6 and 7, and then select the Module 4 assignment
Links to an external site. to try your hand at creating and iterating through sequences. 