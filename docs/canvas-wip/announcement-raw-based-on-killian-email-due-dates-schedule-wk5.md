Several students are confused by the lack of due dates in Canvas. I'm trying to fix that now by copying the Runestone due dates into Canvas. If you have been completing one module per week, you should have module 5 (the midterm exam) done by this Sunday. The due dates in Canvas are all suggestions and you can submit all assignments late, but the further you get behind, the less sense my hints and videos will make sense to you.


**If you are all caught up you would have completed Module 4 last Sunday. We're currently in Week 5 (Module 5) so to keep pace, you want to complete that by this Sunday.**

Here's the course schedule (from the syllabus that was your assigned reading in week 1):

<p><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text">Several students are confused by the lack of due dates in Canvas. Hopefully this announcement will clear things up.</span></span></span></p>
<p><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text"> If you have been completing one module per week, you should have module 5 (the midterm exam) completed by this Sunday. If you are still catching up on assignments, don't worry about this due date -- these due dates in Canvas are merely <em>recommendations</em>. You can submit all assignments late for full credit. But the further you get behind, the less sense my hints and videos will make sense to you.<br /></span></span></span></p>
<p><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text">If you are all caught up you would have <strong>completed M</strong></span></span></span><strong><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text">odule 4 last Sunday. </span></span></span></strong><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text"><span style="color: #000000;">We're currently in Week 5 (Module 5) so to keep pace, you want to complete the midterm exam for <span style="color: #ba372a;"><strong>module 5 by this Sunday.</strong></span></span></span></span></span><strong></strong></p>
<h3><span class="css-ty98t2-view-flexItem" style="color: #34495e;" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text"><strong>Course Schedule (also in the <a title="Syllabus" href="https://sdccd.instructure.com/courses/2467702/assignments/syllabus" data-course-type="navigation">Syllabus</a>)<br /></strong></span></span></span></h3>
<table class="dataframe" border="1">
    <thead>
        <tr style="text-align: right;">
            <th></th>
            <th style="text-align: left;">Due Date</th>
            <th>Assignment</th>
            <th style="text-align: center;">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>0</th>
            <td>2024-03-06</td>
            <td>Ch 1</td>
            <td>Python programming language, variables and types</td>
        </tr>
        <tr>
            <th>1</th>
            <td>2024-03-13</td>
            <td>Ch 2-3</td>
            <td>Debugging and object oriented programming</td>
        </tr>
        <tr>
            <th>2</th>
            <td>2024-03-20</td>
            <td>Ch 4-5</td>
            <td>Sequences and iteration</td>
        </tr>
        <tr>
            <th>3</th>
            <td>2024-03-27</td>
            <td>Ch 6-7</td>
            <td>Indexing, slicing, combining, and iterating through sequences (`list`, `str`, `tuple`)</td>
        </tr>
        <tr>
            <th>4</th>
            <td>2024-04-03</td>
            <td>Ch 1-7</td>
            <td>MIDTERM EXAM - FOPP Chapters 1 - 7</td>
        </tr>
        <tr>
            <th>5</th>
            <td>2024-04-10</td>
            <td>Ch 8-9</td>
            <td>Conditional expressions and modifying sequences</td>
        </tr>
        <tr>
            <th>6</th>
            <td>2024-04-17</td>
            <td></td>
            <td>MIDTERM PROJECT: Text Adventure!</td>
        </tr>
        <tr>
            <th>7</th>
            <td>2024-04-24</td>
            <td>Ch 10-11</td>
            <td>Files and dictionaries</td>
        </tr>
        <tr>
            <th>8</th>
            <td>2024-05-01</td>
            <td>Ch 14-15</td>
            <td>Advanced iteration patterns and functions</td>
        </tr>
        <tr>
            <th>9</th>
            <td>2024-05-08</td>
            <td>Ch 16-17</td>
            <td>Sorting and nestings sequences (`lists`)</td>
        </tr>
        <tr>
            <th>10</th>
            <td>2024-05-15</td>
            <td>Ch 18-19</td>
            <td>Documenting and testing a Python program</td>
        </tr>
        <tr>
            <th>11</th>
            <td>2024-05-22</td>
            <td>Ch 20-21</td>
            <td>Designing an object-oriented (OO) Python program</td>
        </tr>
        <tr>
            <th>12</th>
            <td>2024-05-29</td>
            <td></td>
            <td>FINAL EXAM</td>
        </tr>
        <tr>
            <th>13</th>
            <td>2024-06-05</td>
            <td></td>
            <td>Installing and using external Python packages (`pandas` and `doctest`)</td>
        </tr>
        <tr>
            <th>14</th>
            <td>2024-06-12</td>
            <td></td>
            <td>Data-driven program design</td>
        </tr>
        <tr>
            <th>15</th>
            <td>2024-06-19</td>
            <td></td>
            <td>FINAL PROJECT: A data-driven application.</td>
        </tr>
    </tbody>
</table>
<p><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text">You can complete late assignments for full credit. But if you don't catch up you will have many assignments due at one at the end of the course (in 11 weeks). If you complete them on time you will have 3 weeks to work with me on your final project, which is the most important assignment for your total grade.</span></span></span></p>
<p><span class="css-ty98t2-view-flexItem" dir="ltr"><span class="css-103zv00-view-flexItem" dir="ltr"><span class="css-2dlkjw-text">I'm sorry for the confusion. But don't sweat any of the due dates you see (on Runestone or Canvas). If you submit assignments late I will still give you full credit.</span></span></span></p>


You would have completed week 4 assignments last Sunday, if you were keeping up but I've extended that deadline by an additional week. We're in week 5 (module 5) now, so the week 5 assignments (midterm exam) are technically due this Sunday, but I've extended that deadline one week as well.

You can complete late assignments for full credit. But if you don't catch up you will have many assignments due at one at the end of the course (in 11 weeks). Try to complete more than one module per week until you have caught up.

I'm sorry for the confusion. But don't sweat any of the due dates you see (on Runestone or Canvas). If you submit assignments late I will still give you full credit. But if you wait until too late, you will not be able to take advantage of my hints and help for those module's you are behind on.

I have been putting the assignments in Runestone each week and am behind copying them into Canvas modules, though they've been in the Syllabus from the beginning. So it's my fault too. Try to catch up and make sure you've completed 5 modules this week or module 6 by next Sunday.

I'm reviewing all the assignments this week to make sure they have the correct chapters and due dates on them. The assignments in Runestone have always had the correct due dates and chapters. We're in week 5, so the week 4 assignments were technically due last Sunday, but I've extended that deadline by an additional week. We're in week 5 (module 5) now, so the week 5 assignments (midterm exam) are technically due this Sunday, but I've extended that deadline one week as well. And all assignments can be submitted late for full credit. But try to catch up.