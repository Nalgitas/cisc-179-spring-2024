// SDCCD JS Theme adopted from Pierce College custom javascript - uses the CanUt Canvas library
//   Edited: 07-22-2021


(function() {
    window.prontoInit = {"ixn":"canvas","cid":58,"version":"1.0"};
    var script = document.createElement('script');
    script.src = `https://chat.pronto.io/js/embedded.js?cb=${Math.round(new Date().getTime() / 1000)}`;
    document.body.appendChild(script);
})();


    
$.ajax({ url: "https://d1la9j37oq1fed.cloudfront.net/canvas_shared/canut_base_canvas_050714.min.js", dataType: "script", cache: true, crossDomain: true }).done(function () {
    
// **************************************
    
    //  College custom javascript - Place below this comment block
    
// **************************************


    


    // Hide items we don't want:
    CanUt.HideAccountDelete();
    //CanUt.HideResetCourse();
    CanUt.HideConcludeCourse();
    CanUt.RemoveAllFooterLinks();
    //CanUt.HideGradesMenu();
    //CanUt.HideAssignmentsMenu();



    // Add new items
    CanUt.LoadCanvabadgesScript();


    // Accordion - used by some Pierce faculty
    // Wrap in try-catch statement as  
    // it'll fail if the page doesn't contain a "div.accordion".
    try {
        $('div.accordion').accordion();
    }
    catch (err) { }





    // Footer
    // Originals were removed up above w/ the call to CanUt.RemoveAllFooterLinks()
    //Add new footer content


$('#footer-links').append('<a href="https://www.sdccd.edu/about/departments-and-offices/instructional-services-division/online-learning-pathways-1/students/student%20training.aspx" target="_blank">Are You Ready for Online?</a> ');
$('#footer-links').append('<a href="https://www.sdccd.edu/online/" target="_blank">SDCCD Online Learning Pathways Website</a> ');
$('#footer-links').append('<a href="https://community.canvaslms.com/docs/DOC-10720" target="_blank">Supported Browsers</a>');
    


    if (CanUt.loginPage) {
    //Login info box after footer - only applied to login page      
        $('<div class="loginNote"><STRONG><font color=#FF0000>Login Instructions:</font></STRONG><BR><BR><strong><font color=#FF0000>Students:</font></strong><br/><strong>Username:</strong> 10-Digit Student ID<br><strong>Password:</strong> 8-digit birthdate (MMDDYYYY) <br><strong>Canvas Student Support:</strong> 1-844-612-7421<br/><BR><BR><font color=#FF0000><STRONG>Faculty:</font></strong><br/><strong>Username:</strong> 10-Digit Employee ID<br><strong>Password:</strong> 8-Digit Birthdate (MMDDYYYY) <br/><strong>Canvas Faculty Support:</strong> 1-844-612-7422<br/></div>').insertAfter('#footer');
    }



  $($('#enroll_form>p')[1]).text('Please enter your Canvas username and password');
  $('label[for=student_email]').text('Username');
  $('#forgot_password_instructions').hide();



    // ***********************************************************  
    //  End of custom college javascript
    //
    //  Place all custom college script above this comment block
    //
    // ***********************************************************
});


// Start Pope Tech Accessibility Guide
var popeTechKey='dEeGcZUBv33zBbDE5Ny9FYQmoKEAuvGg';(function(a){function b(a,b){var c=document.createElement("script");c.type="text/javascript",c.readyState?c.onreadystatechange=function(){("loaded"===c.readyState||"complete"===c.readyState)&&(c.onreadystatechange=null,b())}:c.onload=function(){b()},c.src=a,document.getElementsByTagName("head")[0].appendChild(c)}function c(a){return a&&("TeacherEnrollment"===a||"TaEnrollment"===a||"DesignerEnrollment"===a)}function d(){var a=window.location.pathname;return!!(-1!==a.indexOf("/edit")||-1!==a.indexOf("/new")||-1!==a.indexOf("/syllabus")||a.match(/\/courses\/[0-9]+\/pages\/?$/)||a.match(/\/courses\/[0-9]+\/?$/))}function e(){return f()||g()}function f(){var a=/\/courses\/[0-9]+\/pages\/?$/,b=window.location.pathname;return console.log("Check for pages url",window.location.pathname),console.log(a.test(b)),a.test(b)}function g(){var a=window.location.pathname;return console.log("Check for courses url",window.location.pathname),console.log("/courses"===a),"/courses"===a}function h(){var a=/\/accounts\/[0-9]+\/external_tools\/[0-9]+\/?$/,b=/\/courses\/[0-9]+\/external_tools\/[0-9]+\/?$/,c=window.location.pathname;return console.log("Check for external tool url",window.location.pathname),console.log(a.test(c)||b.test(c)),a.test(c)||b.test(c)}function i(f){for(var g=0;g<f.length;++g)if(localStorage.setItem(`${j}.${l}`,JSON.stringify(f)),c(f[g].type)){if(d()&&b("https://canvas-cdn.pope.tech/loader.min.js",function(){}),null===a)break;(e()||h())&&(console.log("Load column"),b(`https://canvas-cdn.pope.tech/accessibility-column.min.js?key=${a}`,function(){}));break}}var j="pt-instructor-guide",k="username",l="enrollments";return-1===window.location.href.indexOf("/login/canvas")?-1===window.location.href.indexOf("?login_success=1")?void function(a,b){var c=localStorage.getItem(`${a}.${b}`);null===c?$.get("/api/v1/users/self/enrollments?type[]=DesignerEnrollment&type[]=TaEnrollment&type[]=TeacherEnrollment",function(a){i(a)}):(c=JSON.parse(c),i(c))}("pt-instructor-guide",l):(localStorage.removeItem(`${"pt-instructor-guide"}.${k}`),void $.get("/api/v1/users/self",function(a){localStorage.setItem(`${"pt-instructor-guide"}.${k}`,a.name)})):(localStorage.removeItem(`${"pt-instructor-guide"}.${k}`),localStorage.removeItem(`${"pt-instructor-guide"}.${"uuid"}`),localStorage.removeItem(`${"pt-instructor-guide"}.${"settings"}`),void localStorage.removeItem(`${"pt-instructor-guide"}.${l}`))})(popeTechKey);
// End Pope Tech Accessibility Guide