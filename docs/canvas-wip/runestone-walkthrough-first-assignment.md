---
title: 'Your first Runestone assignment'
description: 'This walk-through should help you get started on your first graded assignment in Runestone.'
pubDate: 'Feb 6, 2024'
---

# Your first Runestone assignment 

This 7 minute video should help you get started on your first graded assignment in Runestone.
There are also screenshots and instructions below the video, if that makes it easier.

### Video walk-through

This video walks you through the "Module 1 (Chapter 1)" assignment in Runestone and even gives you the answer to the first assignment.

[Video walk-through of "Module 1 (Chapter 1)" assignment](https://www.dropbox.com/scl/fi/u0ctehbvbibzluslm1uhl/runestone-intro-module-1-audio.mp4?rlkey=jh2d4o8i1v137jfnp9s00alpp&dl=0)

### Important!!!

- The interactive code blocks and questions within the textbook do not count towards your grade!
- Your [Choose Assignment](https://runestone.academy/assignment/student/chooseAssignment) page lists your assignments in reverse order, so [Module 1 (Chapter 1)](https://runestone.academy/assignment/student/doAssignment?assignment_id=172579) is listed at the bottom
- Your _Choose Assignment_ page has a pull-down to the right of each assignment to show whether you have completed a chapter.
- Your _Choose Assignment_ page shows your "Self Grade" score to the right of each assignment.
- On your [Module 1 (Chapter 1)](https://runestone.academy/assignment/student/doAssignment?assignment_id=172579), the **Readings** section does not count towards your grade.
- The [Module 1 (Chapter 1)](https://runestone.academy/assignment/student/doAssignment?assignment_id=172579) assignment **Questions** section is below the **Readings** and you will click the "Check my Answer" each time you complete a question.
- You can answer each question and click the check your answer" buttons as many times as you like
- You can click on the "Finished" and "In Progress" buttons as many times as you like. You will get credit for the last (most recent) score you received on the questions.
- At the top (and bottom) of your [Module 1 (Chapter 1)](https://runestone.academy/assignment/student/doAssignment?assignment_id=172579) page you will see a banner with an "In Progress" or "Finished" button highlighted in pink, yellow, or blue. The banner will toggle between three colors:

1. Pink: ![You can mark this assignment as **In Progress** by clicking the button.](https://mesa_python.gitlab.io/images/you-can-mark-this-assignment-as-in-progress.png)
2. Yellow: ![You have marked this assignment as **In Progress**. Click to mark it as Finished.](https://mesa_python.gitlab.io/images/click-to-mark-it-as-finished.png)
3. Green: ![You have marked this assignment as **Finished**. Click to mark it In Progress.](https://mesa_python.gitlab.io/images/click-to-mark-it-in-progress.png)

Once you have completed the assignment make sure it has a green background and the "Self Grade" score shows what you expect.
You can also select the "Finished" menu item from the pull-down menu on your [Choose Assignment](https://runestone.academy/assignment/student/chooseAssignment) page.
