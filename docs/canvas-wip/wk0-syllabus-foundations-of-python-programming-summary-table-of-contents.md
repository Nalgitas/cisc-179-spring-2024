# Syllabus: Intro Python Programming

**Name:** Computer and Information Sciences (CISC) 179 - Programming

**Course:** Computer and Information Sciences (CISC) 179 - CRN 91377

**Instructor:** Hobson Lane

**E-mail:** lane@totalgood.com

### Description

This is an introductory course in Python programming, incorporating the fundamentals of object oriented software and user interface design. You will learn how to program a computer to interact with computer input and output devices to create a user interface for controlling that program. You will learn how to:

- Analyze user needs and requirements
- Design a user interface for a Python application
- Create classes with attributes and methods for implementing your program
- Write software for event procedures and business logic
- Test and debug the completed programs and applications
- Document your program for users and developers using Python "doctests"

This course is intended for Computer and Information Sciences majors or anyone interested in the Python programming language.

### Important dates

- 01/29/2024 - Start of Spring Semester
- 02/09/2024 - Drop deadline (drops with full refund and no "W" on transcript)
- 02/16/2024 - 02/19/2024 - Lincoln/Washington Day (CAMPUS CLOSED)
- 03/25/2024 - 03/29/2024 - (SPRING BREAK)
- 03/29/2024 - Cesar Chavez Day (CAMPUS CLOSED)
- 04/12/2024 - Withdrawal Deadline (Primary 16-Wk Session)
- 04/30/2024 - Deadline to Apply for Graduation
- 05/25/2024 - End of Spring Semester

### Learning goals

1. Use conditional boolean expressions to control program flow.
2. Use `if`...`else` structures to solve a problem.
3. Use a `for` or `while` loop to perform a sequence of actions.
4. Use predefined (hard-coded) `list`, `str`, `float`, `dict`, `bool`, and `int` variables to contol program flow (data-driven software).
5. Use `input` and `output` to interact with the user through the console (terminal).
6. Use `open`, `read`, amd `write` to read and write strings to a file. 
7. Design and implement a Python program that solves a problem using 1-6 above.

### Interactive Textbook: _Fundamentals of Python Programming_

Important sections from the textbook _Fundamentals of Python Programming_ on Runestone.

- [1\. General Introduction](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/toctree.html)
    - [1.2. Algorithms](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/Algorithms.html)
    - [1.3. The Python Programming Language](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/ThePythonProgrammingLanguage.html)
    - [1.6. Formal and Natural Languages](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/FormalandNaturalLanguages.html)
    - [1.7. A Typical First Program](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/ATypicalFirstProgram.html)
    - [1.8. Predict Before You Run!](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/WPPredictBeforeYouRun.html) <== **ACTIVE LEARNING!!**
- [2\. Variables, Statements, and Expressions](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/toctree.html)
    - [2.2. Values and Data Types](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/Values.html)
    - [2.3. Operators and Operands](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/Operators.html)
    - [2.4. Function Calls](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/FunctionCalls.html)
    - [2.7. Variables](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/Variables.html)
    - [2.10. Statements and Expressions](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/StatementsandExpressions.html)
    - [2.11. Order of Operations](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/OrderofOperations.html)
    - [2.13. Updating Variables](https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/UpdatingVariables.html)
- [3\. Debugging](https://runestone.academy/ns/books/published/mesa_python/Debugging/toctree.html)
    - [3.3. 👩🏾🖥️ Debugging](https://runestone.academy/ns/books/published/mesa_python/Debugging/intro-HowtobeaSuccessfulProgrammer.html#debugging)
    - [3.8. 👩🏾🖥️ Know Your Error Messages](https://runestone.academy/ns/books/published/mesa_python/Debugging/KnowyourerrorMessages.html)
- [4\. Python Modules](https://runestone.academy/ns/books/published/mesa_python/PythonModules/toctree.html)
    - [4.2. Modules](https://runestone.academy/ns/books/published/mesa_python/PythonModules/intro-ModulesandGettingHelp.html)
    - [4.3. The `random` module](https://runestone.academy/ns/books/published/mesa_python/PythonModules/Therandommodule.html)
- [5\. Python Turtle](https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/toctree.html)
    - [5.3. Instances: A Herd of Turtles](https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/InstancesAHerdofTurtles.html)
    - [5.4. Object Oriented Concepts](https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/ObjectInstances.html)
    - [5.5. Repetition with a For Loop](https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/RepetitionwithaForLoop.html)
    - [5.8. 👩🏾🖥️ Incremental Programming](https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/WPIncrementalProgramming.html) <== **Agile!!**
- [6\. Sequences](https://runestone.academy/ns/books/published/mesa_python/Sequences/toctree.html)
    - [6.2. Strings and Lists](https://runestone.academy/ns/books/published/mesa_python/Sequences/StringsandLists.html)
    - [6.3. Index Operator: Working with the Characters of a String](https://runestone.academy/ns/books/published/mesa_python/Sequences/IndexOperatorWorkingwiththeCharactersofaString.html)
    - [6.6. The Slice Operator](https://runestone.academy/ns/books/published/mesa_python/Sequences/TheSliceOperator.html)
    - [6.7. Concatenation and Repetition](https://runestone.academy/ns/books/published/mesa_python/Sequences/ConcatenationandRepetition.html)
    - [6.8. Count and Index](https://runestone.academy/ns/books/published/mesa_python/Sequences/CountandIndex.html)
    - [6.9. Splitting and Joining Strings](https://runestone.academy/ns/books/published/mesa_python/Sequences/SplitandJoin.html)
- [7\. Iteration](https://runestone.academy/ns/books/published/mesa_python/Iteration/toctree.html)
    - [7.2. The **for** Loop](https://runestone.academy/ns/books/published/mesa_python/Iteration/TheforLoop.html)
    - [7.5. Lists and `for` loops](https://runestone.academy/ns/books/published/mesa_python/Iteration/Listsandforloops.html)
    - [7.6. The Accumulator Pattern](https://runestone.academy/ns/books/published/mesa_python/Iteration/TheAccumulatorPattern.html)
    - [7.8. Nested Iteration: Image Processing](https://runestone.academy/ns/books/published/mesa_python/Iteration/NestedIterationImageProcessing.html)
    - [7.9. 👩🏾🖥️ Printing Intermediate Results](https://runestone.academy/ns/books/published/mesa_python/Iteration/WPPrintingIntermediateResults.html)
- [8\. Conditionals](https://runestone.academy/ns/books/published/mesa_python/Conditionals/toctree.html)
    - [8.2. Boolean Values and Boolean Expressions](https://runestone.academy/ns/books/published/mesa_python/Conditionals/BooleanValuesandBooleanExpressions.html)
    - [8.4. The `in` and `not in` operators](https://runestone.academy/ns/books/published/mesa_python/Conditionals/Theinandnotinoperators.html)
    - [8.5. Precedence of Operators](https://runestone.academy/ns/books/published/mesa_python/Conditionals/PrecedenceofOperators.html)
    - [8.7. Omitting the `else` Clause: Unary Selection](https://runestone.academy/ns/books/published/mesa_python/Conditionals/OmittingtheelseClauseUnarySelection.html)
    - [8.8. Nested conditionals](https://runestone.academy/ns/books/published/mesa_python/Conditionals/Nestedconditionals.html)
    - [8.10. The Accumulator Pattern with Conditionals](https://runestone.academy/ns/books/published/mesa_python/Conditionals/TheAccumulatorPatternwithConditionals.html)
- [9\. Transforming Sequences](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/toctree.html)
    - [9.3. List Element Deletion](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/ListDeletion.html)
    - [9.4. Objects and References](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/ObjectsandReferences.html)
    - [9.7. Mutating Methods](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/MutatingMethods.html)
    - [9.8. Append versus Concatenate](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/AppendversusConcatenate.html)
    - [9.11. The Accumulator Pattern with Strings](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/TheAccumulatorPatternwithStrings.html)
    - [9.13. 👩🏾🖥️ Don’t Mutate A List That You Are Iterating Through](https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/WPDontMutateAListYouIterateThrough.html)
- [10\. Files](https://runestone.academy/ns/books/published/mesa_python/Files/toctree.html)
    - [10.2. Reading a File](https://runestone.academy/ns/books/published/mesa_python/Files/ReadingaFile.html)
    - [10.3. Alternative File Reading Methods](https://runestone.academy/ns/books/published/mesa_python/Files/AlternativeFileReadingMethods.html)
    - [10.4. Iterating over lines in a file](https://runestone.academy/ns/books/published/mesa_python/Files/Iteratingoverlinesinafile.html)
    - [10.5. Finding a File in your Filesystem](https://runestone.academy/ns/books/published/mesa_python/Files/FindingaFileonyourDisk.html)
    - [10.6. Using `with` for Files](https://runestone.academy/ns/books/published/mesa_python/Files/With.html)
    - [10.7. Recipe for Reading and Processing a File](https://runestone.academy/ns/books/published/mesa_python/Files/FilesRecipe.html)
    - [10.8. Writing Text Files](https://runestone.academy/ns/books/published/mesa_python/Files/WritingTextFiles.html)
    - [10.10. Reading in data from a CSV File](https://runestone.academy/ns/books/published/mesa_python/Files/ReadingCSVFiles.html)
- [11\. Dictionaries](https://runestone.academy/ns/books/published/mesa_python/Dictionaries/toctree.html)
    - [11.2. Getting Started with Dictionaries](https://runestone.academy/ns/books/published/mesa_python/Dictionaries/intro-Dictionaries.html)
    - [11.5. Aliasing and copying](https://runestone.academy/ns/books/published/mesa_python/Dictionaries/Aliasingandcopying.html)
    - [11.8. Accumulating the Best Key](https://runestone.academy/ns/books/published/mesa_python/Dictionaries/AccumulatingtheBestKey.html)
- [12\. Functions](https://runestone.academy/ns/books/published/mesa_python/Functions/toctree.html)
    - [12.2. Function Definition](https://runestone.academy/ns/books/published/mesa_python/Functions/FunctionDefinitions.html)
    - [12.4. Function Parameters](https://runestone.academy/ns/books/published/mesa_python/Functions/FunctionParameters.html)
    - [12.5. Returning a value from a function](https://runestone.academy/ns/books/published/mesa_python/Functions/Returningavaluefromafunction.html)
    - [12.9. Variables and parameters are local](https://runestone.academy/ns/books/published/mesa_python/Functions/Variablesandparametersarelocal.html)
    - [12.10. Global Variables](https://runestone.academy/ns/books/published/mesa_python/Functions/GlobalVariables.html)
    - [12.13. 👩🏾🖥️ Print vs. return](https://runestone.academy/ns/books/published/mesa_python/Functions/Printvsreturn.html)
    - [12.14. Passing Mutable Objects](https://runestone.academy/ns/books/published/mesa_python/Functions/PassingMutableObjects.html)
- [13\. Tuple Packing and Unpacking](https://runestone.academy/ns/books/published/mesa_python/Tuples/toctree.html)
    - [13.2. Tuple Packing](https://runestone.academy/ns/books/published/mesa_python/Tuples/TuplePacking.html)
    - [13.3. Tuple Assignment with Unpacking](https://runestone.academy/ns/books/published/mesa_python/Tuples/TupleAssignmentwithunpacking.html)
    - [13.4. Tuples as Return Values](https://runestone.academy/ns/books/published/mesa_python/Tuples/TuplesasReturnValues.html)
    - [13.5. Unpacking Tuples as Arguments to Function Calls](https://runestone.academy/ns/books/published/mesa_python/Tuples/UnpackingArgumentsToFunctions.html)
- [14\. More About Iteration](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/toctree.html)
    - [14.2. The `while` Statement](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/ThewhileStatement.html)
    - [14.3. The Listener Loop](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/listenerLoop.html)
            - [14.3.1.1. Sentinel Values](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/listenerLoop.html#sentinel-values)
            - [14.3.1.2. Validating Input](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/listenerLoop.html#validating-input)
    - [14.4. Randomly Walking Turtles](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/RandomlyWalkingTurtles.html)
    - [14.5. Break and Continue](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/BreakandContinue.html)
    - [14.6. 👩🏾🖥️ Infinite Loops](https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/WPInfiniteLoops.html)

### Advanced Python programming

- [15\. Advanced Functions](https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/toctree.html)
    - [15.2. Keyword Parameters](https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/KeywordParameters.html)
    - [15.3. Anonymous functions with lambda expressions](https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/Anonymousfunctionswithlambdaexpressions.html)
    - [15.4. 👩🏾🖥️ Programming With Style](https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/ProgrammingWithStyle.html)
    - [15.5. Method Invocations](https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/MethodInvocations.html)
- [16\. Sorting](https://runestone.academy/ns/books/published/mesa_python/Sorting/toctree.html)
    - [16.2. Optional reverse parameter](https://runestone.academy/ns/books/published/mesa_python/Sorting/Optionalreverseparameter.html)
    - [16.3. Optional key parameter](https://runestone.academy/ns/books/published/mesa_python/Sorting/Optionalkeyparameter.html)
    - [16.4. Sorting a Dictionary](https://runestone.academy/ns/books/published/mesa_python/Sorting/SortingaDictionary.html)
    - [16.5. Breaking Ties: Second Sorting](https://runestone.academy/ns/books/published/mesa_python/Sorting/SecondarySortOrder.html)
    - [16.6. 👩🏾🖥️ When to use a Lambda Expression](https://runestone.academy/ns/books/published/mesa_python/Sorting/WPWhenToUseLambdaVsFunction.html)
- [17\. Nested Data and Nested Iteration](https://runestone.academy/ns/books/published/mesa_python/NestedData/toctree.html)
    - [17.2. Nested Dictionaries](https://runestone.academy/ns/books/published/mesa_python/NestedData/NestedDictionaries.html)
    - [17.3. Processing JSON results](https://runestone.academy/ns/books/published/mesa_python/NestedData/jsonlib.html)
    - [17.4. Nested Iteration](https://runestone.academy/ns/books/published/mesa_python/NestedData/NestedIteration.html)
    - [17.5. 👩🏾🖥️ Structuring Nested Data](https://runestone.academy/ns/books/published/mesa_python/NestedData/WPStructuringNestedData.html)
    - [17.6. Deep and Shallow Copies](https://runestone.academy/ns/books/published/mesa_python/NestedData/DeepandShallowCopies.html)
    - [17.7. 👩🏾🖥️ Extracting from Nested Data](https://runestone.academy/ns/books/published/mesa_python/NestedData/WPExtractFromNestedData.html)
- [18\. Test Cases](https://runestone.academy/ns/books/published/mesa_python/TestCases/toctree.html)
    - [18.2. Checking Assumptions About Data Types](https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingTypes.html)
    - [18.3. Checking Other Assumptions](https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingTypes.html#checking-other-assumptions)
    - [18.5. Testing Loops](https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingLoops.html)
    - [18.6. Writing Test Cases for Functions](https://runestone.academy/ns/books/published/mesa_python/TestCases/Testingfunctions.html)
    - [18.7. Testing Optional Parameters](https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingOptionalParameters.html)
- [19\. Exceptions](https://runestone.academy/ns/books/published/mesa_python/Exceptions/toctree.html)
    - [19.2. Exception Handling Flow-of-control](https://runestone.academy/ns/books/published/mesa_python/Exceptions/intro-exceptions.html#exception-handling-flow-of-control)
    - [19.3. 👩🏾🖥️ When to use try/except](https://runestone.academy/ns/books/published/mesa_python/Exceptions/using-exceptions.html)
    - [19.4. Standard Exceptions](https://runestone.academy/ns/books/published/mesa_python/Exceptions/standard-exceptions.html)
- [20\. Defining your own Classes](https://runestone.academy/ns/books/published/mesa_python/Classes/toctree.html)
    - [20.3. User Defined Classes](https://runestone.academy/ns/books/published/mesa_python/Classes/UserDefinedClasses.html)
    - [20.4. Adding Parameters to the Constructor](https://runestone.academy/ns/books/published/mesa_python/Classes/ImprovingourConstructor.html)
    - [20.5. Adding Other Methods to a Class](https://runestone.academy/ns/books/published/mesa_python/Classes/AddingOtherMethodstoourClass.html)
    - [20.8. Instances as Return Values](https://runestone.academy/ns/books/published/mesa_python/Classes/InstancesasReturnValues.html)
    - [20.9. Sorting Lists of Instances](https://runestone.academy/ns/books/published/mesa_python/Classes/sorting_instances.html)
    - [20.13. A Tamagotchi Game](https://runestone.academy/ns/books/published/mesa_python/Classes/Tamagotchi.html)
- [21\. Building Programs](https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/toctree.html)
    - [21.1. Building A Program: A Strategy](https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/TheStrategy.html)
    - [21.2. 👩🏾🖥️ Sketch an Outline](https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/WPSketchanOutline.html)
    - [21.3. 👩🏾🖥️ Code one section at a time](https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/WPCodeSectionataTime.html)
    - [21.4. 👩🏾🖥️ Clean Up](https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/WPCleanCode.html)
- [22\. Inheritance](https://runestone.academy/ns/books/published/mesa_python/Inheritance/toctree.html)
    - [22.2. Inheriting Variables and Methods](https://runestone.academy/ns/books/published/mesa_python/Inheritance/inheritVarsAndMethods.html)
    - [22.3. Overriding Methods](https://runestone.academy/ns/books/published/mesa_python/Inheritance/OverrideMethods.html)
    - [22.4. Invoking the Parent Class’s Method](https://runestone.academy/ns/books/published/mesa_python/Inheritance/InvokingSuperMethods.html)
    - [22.5. Tamagotchi Revisited](https://runestone.academy/ns/books/published/mesa_python/Inheritance/TamagotchiRevisited.html)
    - [22.8. Project - Wheel of Python](https://runestone.academy/ns/books/published/mesa_python/Inheritance/chapterProject.html)
- [23\. More on Accumulation: Map, Filter, List Comprehension, and Zip](https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/toctree.html)
    - [23.2. Map](https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/map.html)
    - [23.3. Filter](https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/filter.html)
    - [23.4. List Comprehensions](https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/listcomp.html)
    - [23.5. Zip](https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/zip.html)
    - [23.7. Chapter Assessment](https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/ChapterAssessment.html)
