Click on the dark gray profile icon in the upper right of the Runestone
page and select the "Register" menu item:

![](images/runestone-register.png)<!-- {width="5.6047in" height="4.0547in"} -->
Here's where you can submit your projects.

https://gitlab.com/tangibleai/community/cs179/
