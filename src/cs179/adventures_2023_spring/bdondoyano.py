print("Welcome to the Melting Pot! Today we are going to make a conglomerate of my favorite cuisines. You will pick a ")
print("random number, each number associated with a kind of food. You will go through five courses to detemine your ")
print("end dish. Best of luck,I hope it tastes good :p")

#Drink

cuisines = ["asian", "mexican", "bbq", "italian"]
dishes = ["drink", "protein", "carb", "veggie", "dessert"]

asian_drink = "sake"
mexican_drink = "michelada"
bbq_drink = "tangy lemonade"
italian_drink = "red wine"

print("")
print("Your first order will be your drink.")
drink = int(input("Pick a number between 1 and 4: "))

if drink == 1:
      print("Your drink is", asian_drink)
      drink = asian_drink
if drink == 2:
      print("Enjoy some", mexican_drink)
      drink = mexican_drink
if drink == 3:
      print("Some", bbq_drink, "for you.")
      drink = bbq_drink
if drink == 4:
      print("El vino for dinner,", italian_drink, "to be exact")
      drink = italian_drink

print("")

protein = int(input("Your next order is your protein, pick 1-4: "))

asian_protein = "teriyaki chicken"
mexican_protein = "carne asada"
italian_protein = "italian sausage"
bbq_protein = "bbq ribs"

if protein == 2:
      print("Your meat is", asian_protein)
      protein = asian_protein
if protein == 4:
      print("Enjoy some", mexican_protein)
      protein = mexican_protein
if protein == 3:
      print("Some", bbq_protein, "for you.")
      protein = bbq_protein
if protein == 1:
      print("Meatball for dinner,", italian_drink, "to be exact")
      protein = italian_protein

print("")

carb = int(input("Next up, let's get starchy, pick 1-4: "))

asian_carb = "white rice"
mexican_carb = "tortilla"
italian_carb = "spaghetti"
bbq_carb = "corn bread"

if carb == 1:
      print("Your carb is", asian_carb)
      carb = asian_carb
if carb == 3:
      print("Enjoy some", mexican_carb)
      carb = mexican_carb
if carb == 4:
      print("Some", bbq_carb, "for you.")
      carb = bbq_carb
if carb == 2:
      print("Time for some noodles,", italian_carb, "to be exact")
      carb = italian_carb

print("")

veggie = int(input("Can't forget our greens, pick 1-4: "))

asian_veggie = "edamame"
mexican_veggie = "peppers"
italian_veggie = "side salad"
bbq_veggie = "corn"

if veggie == 4:
      print("Your veggie is", asian_veggie)
      veggie = asian_veggie
if veggie == 3:
      print("Enjoy some", mexican_veggie)
      veggie = mexican_veggie
if veggie == 2:
      print("Some", bbq_veggie, "for you.")
      veggie = bbq_veggie
if veggie == 1:
      print("How about them leaves,", italian_veggie, "to be exact")
      veggie = italian_veggie

print("")

dessert = int(input("Last but not least, for your sweet tooth, pick 1-4: "))

asian_dessert = "mochi ice cream"
mexican_dessert = "bunuelo"
italian_dessert = "lava cake"
bbq_dessert = "horchata"

if dessert == 2:
      print("Your dessert is", asian_dessert)
      dessert = asian_dessert
if dessert == 4:
      print("Enjoy some", mexican_dessert)
      dessert = mexican_dessert
if dessert == 1:
      print("Some", bbq_dessert, "for you.")
      dessert = bbq_dessert
if dessert == 3:
      print("ooooh yeah,", italian_dessert)
      dessert = italian_dessert

print("Wow what a meal, you got", drink, "to throw back, some", protein, "with a side of", carb, "and", veggie, ". All")
print("before you finish off with some", dessert,". Bon Appetit!")