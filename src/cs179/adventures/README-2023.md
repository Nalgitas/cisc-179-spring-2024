Over the next 3 weeks you will be building a text adventure game. The sooner you get started the better your game will turn out!

![design_cave_adventure.drawio.svg](https://sdccd.instructure.com/courses/2467702/files/147515834/preview)

## Choose your own text adventure

Have you ever played a text adventure or role playing game? In role playing, your friend acts as the Dungeon Master (DM). The DM describes a world to you and your \_party\_ as you embark on a \_campaign\_ or adventure. Together your \_party\_ answers the DM's questions and the DM decides what happens next. In a text adventure game, a computer program is the Dungeon Master. And the computer describes the world with text rather than voice (and hand waving;).

#### **IMPORTANT:** [**STEP-BY-STEP INSTRUCTIONS (including example code) HERE** Links to an external site.](https://gitlab.com/mesa_python/cisc-179-spring-2024/-/tree/main/src/cs179/adventures)

## Colossal Cave Adventure

To help get you started, here's the opening scene in the very first text adventure ever created:

```
<span id="LC1" lang="plaintext">WELCOME TO ADVENTURE!!  WOULD YOU LIKE INSTRUCTIONS?</span>
```

_`no`_

```
<span id="LC1" lang="plaintext">YOU ARE STANDING AT THE END OF A ROAD BEFORE A SMALL BRICK BUILDING. AROUND YOU IS A FOREST. A SMALL STREAM FLOWS OUT OF THE BUILDING AND DOWN A GULLY.</span>
<span id="LC2" lang="plaintext">YOU HAVE WALKED UP A HILL, STILL IN THE FOREST THE ROAD NOW SLOPES BACK DOWN THE OTHER SIDE OF THE HILL. THERE IS A BUILDING IN THE DISTANCE.</span>
```

_`east`_

```
<span id="LC1" lang="plaintext">YOU ARE INSIDE A BUILDING, A WELL HOUSE FOR A LARGE SPRING.</span>
<span id="LC2" lang="plaintext"></span>
<span id="LC3" lang="plaintext">THERE ARE SOME KEYS ON THE GROUND HERE.</span>
<span id="LC4" lang="plaintext"></span>
<span id="LC5" lang="plaintext">HERE IS A SHINY BRASS LAMP NEARBY.</span>
<span id="LC6" lang="plaintext"></span>
<span id="LC7" lang="plaintext">THERE IS FOOD HERE.</span>
```

### . . .

## **YOUR** Adventure

A lot of great programmers (like Willie Crowthers) got their start by building text adventures. **Your mission, if you chose to accept it, is to create a text adventure.** Your Python program only needs to be able to print things to the terminal screen and take input from the user.

The fun part is creating the rooms and levels of the game - the _text_ in your text adventure. But if this assignment is too fun for you, and you'd rather build something more serious, think about how your program could be a virtual assistant. Text-based programs like this are the brains behind popular virtual assistants like [MyCroft.AI Links to an external site.](https://mycroft.ai/), You.com, and [Qary.ai Links to an external site.](https://qary.ai/). So feel free to turn your game into a "virtual assistant" or a simple chatbot.

The only requirements for this exercise are that your program:

-   Has 5 or more "rooms" (called _states_ in computer science)
-   Understands 4 or more commands (_keywords_)
-   Allows the user to visit all rooms
-   Allows the user to reach the end -- your program should eventually _halt_ or exit (but **NOT** [HCF Links to an external site.](https://en.wikipedia.org/wiki/Halt_and_Catch_Fire_(computing)) `;-)`

### Programming Roadmap

For every programming project you will want to do these ten things:

1.  _Ideation_ - think of an idea for your program
2.  Sketch a _wireframe_ - think about what your program looks like when it runs
3.  Write [_pseudocode_ Links to an external site.](https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/Algorithms.html) for a small piece of the program
4.  Translate the _pseudocode_ into Python code (write some Python code)
5.  Test the program yourself (_developer testing_)
6.  Go to step 4 to _fix bugs_ until your program works
7.  Go to step 3 to _add a feature_ that makes your program a little better... until the program is minimally useful
8.  Release or share your program

#### **IMPORTANT: CONTINUE READING THE** [**STEP-BY-STEP INSTRUCTIONS (with example python code) HERE** Links to an external site.](https://gitlab.com/tangibleai/inactive/cs179/-/tree/main/src/cs179/adventures)

| Due | For | Available from | Until |
| --- | --- | --- | --- |
| Mar 17 at 11:59pm | Everyone | Feb 25 at 12am | N/A |