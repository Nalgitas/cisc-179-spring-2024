story = "Lore.txt"
outfile = open(story, "w")


outfile.write("start:You are a young adventurer named Ezreal, your parents are the most notable explorers of Runeterra.\n"
"Growing up all you knew were fond memories, running around your parent’s museum or having family dinners every night.\n "
"You couldn’t have imagined a better life… That was until your eighteenth birthday,\n"
"you were waiting at home for your parents to return from their big expedition in Shurima. \n"
"At first you just thought they were late but after days start to go by, and you start to worry, you ask around but still no answer. \n"
"No one has seen them since their departure weeks ago. \nTired of waiting, \n"
"you set out all your gear and as you take a step out of your childhood home, \n"
"you look back and hope that the next time you return your parents are there next to you.\n"
"Ten days go by...\n \nAs you enter Shurima, the air is hot and heavy. Shurima was known as the largest empire one hundred  years ago, However as quickly as it rose to power it fell just as fast. \n"
"The Emperor in its peak was Azir, the Ascended, King of Men. \n"
"He was known as the richest man in Runeterra with all his riches buried with him in his tomb. \n"
"With the promise of a lifetime of wealth many have ventured into the tomb, with none of them ever seeing the light of day again.\n\n"
"Many assumed that that is the reason his parents ventured into the tomb as well. "
"But what many didn’t know is that there was a secret treasure hidden in the tomb as well… the legendary artifact of Nasus, \n"
"said to give the finder limitless knowledge of past, present and future. And that is what his parents were after, \n"
"the power to make this world a better place to expand and help all people.\n"
"Ezeral didn’t care for the artifact as much as his parents did but if he could find both it would be great.\n\n"
"As he approached the tomb it looked as if it was untouched, towering over him is a dune of sand with a small entryway in the front..\n"



"1: As you pass the entryway, a stone door starts to slide closed behind you. You rush back to stop it but it was too late. \n"
"It seems like there's only one direction to go in now. As you turn to walk down the corridor, you grab a lit torch off the wall and proceed down the dusty hall.\n"
"As you walk you notice that the hall splits into two pathways. To the left you hear, a loud growl,\n "
"of what you can imagine is a vicious creature. To the right, you see an opened steel gate with a skeleton lying against a massive black door.\n" 

"2: As you walk around the temple you see something glimmering sticking out of the sand.\n "
"You dust the sand away and it is a mirror. Upon closer inspection you recognize it's your mothers keepsake. \n"
"Dread fills your senses and you start to dig deeper to find any other clue as to their whereabouts. \n"
"As you dig it reveals a trapdoor embedded into the ground. You slide the door open and you see a ladder that goes down,\n "
"at the bottom you spot a light. You have no idea where this tunnel could lead to.. Do you go down the hatch or go back to the entrance?\n"

"3: You creep slowly down the hall, the growls grow louder as you get closer.\n "
"Until you hit an opening, back against the wall you peek over the corner to see a large sand monster in the middle of the room. \n"
"You start to notice that he isn’t moving and assume those low growls were actually snores. As you scan the rest of the room, \n"
"you see an open door across the room to the north. A table with piles of gold and treasure and what looks to be a jeweled dagger \n"
", to the west of the entance. While the gold is enticing, it would require sneaking past the monster to get there.\n "
"While the only way to make it to the door before the monster catches up.\n "

              
"4: As to walk towards the gate you hear a creaking noise. \n"
"You turn around but don't see anything, thinking nothing of it you continue through the gate and …. \n"
"BANG…. Shocked to turn around and that skeleton that was hunched over is now towering over you!! \n"
"As you try to unscramble your brain you realize he had closed the gate behind you and is leaving you with two options either run or fight\n"


"5.As you descend the hatch a warm breeze rushes past you. Your feet hit the floor and you realize it has become difficult to walk. \n"
"You go to reach your hand down to your boots but something catches your hand.\n "
"Before you can react your other hand becomes entrapped, you try to rip yourself free but the hold only tightens.\n "
"You hear a strange sinister voice, “My webs are absolute, once they've chosen my prey there is no escape.” \n"
"The light at the end creeps slowly towards you, revealing a room filled almost completely with webs. \n"
"As it approaches you see what looks like a woman holding a lantern. As she nears you realize it is no woman, \n"
"instead a monstrous hybrid of a woman with a spider's body attached at her hips. She still seems to be half-human, \n"
"you could choose to try and convince her or try to escape your demise.\n"

              
"6.As you turn back, you hear loud crashing footsteps coming towards you in the darkness.\n "
"Panicked, you try to run the other way only to stumble and fall. \n"
"The footsteps get closer and closer until they are right on top of you. \n"
"To your surprise you feel a hard kick and then hear a man trip and fall over you. \n"
"You both quickly get up and the man raises his torch to see you.\n "
"You notice that it's Renekton, a close friend of your parents who routinely ventured with them. \n"
"However he looks shaken and terrified of something. “I can't go back, I need to leave this place, this isn't knowledge.”\n "
"he stutters out. You notice an orange glowing gem clutched in his hands.\n"

              
            
"7: You quietly tread around the sand monster making sure to keep track of your footing. \n"
"As you approach the table you reach for the dagger, \n"
"however upon pulling the dagger out of the pile the table loses balance and all of the gold spills onto the floor with a loud clatter. \n"
"You quickly turn with the dagger clutched in hand only to be face to face with the viscous sand monster.\n"

"7a: The sand monster lets out a loud shriek with its jaws wide open as it lunges at you.\n "
"You quickly dodge out of the way as it collides with the table behind you. \n"
"You see the door and your mind goes running for it as fast as possible,\n "
"but as if the monster could read your thoughts it planted itself directly in between you and the door.\n"
" It lunges again at you and this time too quickly for you to dodge. You fall to the floor with it on top of you as its jaws viciously snapping as you desperately try to hold it back. \n"
"You reach for the jeweled dagger you had grabbed and blindly thrust it as hard as you can into the monster's side. \n"
"In an instant the creature stopped snapping its gaping jaws and grew limp.\n"
"You push it off of you and head to the door before anything can follow you.\n"

"8 Thinking of how close your parents could be you decide to head for the door, \n"
"disregarding the monster’s guarded treasures. You carefully sneak to the open doorway, \n"
"keeping track of the monster's snores to make sure it's still sleeping. \n"
"As you encroach on the doorway you notice a lever on the wall, likely to close the door behind you.\n "
"You worry you may be closing off your only exit, \n"
"however while nervous about your choice you realize the room is dead silent and the monster is now staring directly at you with ravenous eyes.\n"
"You immediately pull the lever and watch the heavy stone door close shut.\n"

"9:As you run, you twist and turn at every corner to try to out speed the skeleton. \n"
"No matter how fast you sprint you still hear the creaking bones behind you.\n "
"You turn a corner and run straight into a dead end. Before you can turn back you already hear the approaching clank of bones.\n"
"With your back against the wall you feel a loose brick on your hand, \n"
"you press the brick and the wall quickly swivels around putting you in a different room with the faint sound of bones on the other side of the wall.\n"


"10: As the huge skeleton slowly moves towards you,\n "
"you frantically search the corridor for any kind of weapon to defend yourself. \n"
"You only seem to find several small stones around your feet, until you notice the dented shield against the wall behind the skeleton.\n "
"Before you can think the skeleton lunges towards you with its sword raised above its head. \n"
"You quickly dodge behind it and grab the shield, but the skeleton is already readying his next blow.\n "
"Without hesitation you slam the shield into the skeleton's torso pinning him to the wall. \n"
"To your amazement his ribcage crumbles to dust and the rest of him follows shortly after.\n"
              
"10a: You notice atop the pile of dust is a large iron key. \n"
"You pick up the key and head to the ominous black door,\n "
"the key fits perfectly into the keyhole and the door opens with a loud creak.\n"          

"11:  As she nears you can clearly see the face of what could've been a beautiful woman, however it's now deformed and spider-like. \n"
"“You’re a lot younger than the other unfortunate grave robbers that I've caught.” she says as she caresses your cheek.\n "
"You exclaim you aren’t robbing anyone and only here to find your parents. She stares at you puzzled for a moment before realizing that you're unarmed and dressed in fine clothes.\n"
" You quickly explain that your parents went searching for the artifact of Nasus. \n"
"Before you can finish you notice the woman beginning to sob. \n"
"“The legends are a lie!” she yells, “It does not give knowledge it only curses.” \n"
"She explains her name is Elise and she was once an adventurer hundreds of years ago seeking the same fortune as your parents. \n"
"However after finding and removing the artifact her body began to twist and contort and she awoke the way she is now.\n"
" “Please the artifact is cursed, please return it to free me.” she says as she frees you from her webs. \n"
"She places an ominous orange glowing gem in your hand.\n "
"“Be careful you’re my only hope, and the others that have been cursed have lost most of their mind.” she says. \n"
"You agree and with gem in hand you head through a small tunnel.\n"
              
"12: You see the light creep closer and closer, it reveals countless drained corpses entrapped in webs.\n"
"You panic searching yourself with anything to defend yourself. You remember your mothers mirror you had placed in your pocket,this may be your only form of defense.\n "
"You struggle and pull at your enwebbed hand with all your might, you feel it getting loose, but the light is now right infront of you.\n"
"You see that it's a flimsy lantern, however the monstrosity holding it had frozen you in fear. As it reaches its claws towards your face you finally break your hand free.\n "
"Grabbing the mirror you slam it into the monster's arm the glass shatters and tears through its flesh. You seize this moment to free your other limbs, as you finally break hold of the webs you turn to see the monster shrieking in pain.\n"
" You glance at the lantern that it had dropped which had broken spouting fire onto the flammable webs. Before you can react the flames erupt into an inferno in front of you.\n "
"You turn to see a small tunnel dug into the wall, and to and to avoid the oncoming sea of flames you dive in blindly.\n"

"13: You yell for him to concentrate and to explain what happened, and where are you parents. \n"
"“Through there.” he mutters pointing towards a dark hallway. As you turn to head down the hallway he stops you.\n "
"“This place is cursed, it's all a lie no one should be down here.” he stretches out his hand giving you the strange orange gem.\n"
" “I don't want anything to do with this place.” he says fearfully before running off. \n"
"With the gem in your grasp you reluctantly head down the hall.\n"

"14: Renekton frantically looked around as if  not knowing where to go or what to do. \n"
"You forcefully grab his arm and rip the gem from his hand.\n "
"You start to run towards the hallway he came from expecting him to pursue you,\n"
" however instead of the footsteps following you they grew fainter. \n"
"You turn to see him running in the opposite direction and watch as his torch fades into the darkness.\n"
" You know something isn't right but you also know that your parents need your help.\n"
         
              
"15: As you continue you walk, you enter a chamber room with hieroglyphs covering the walls from top to bottom. \n"
"You see a giant metallic door at the end of the room with a small hole in the center."
" In the center of the room is a podium made of gold, you see Shurimian letters engraved in the side and with little skill, "
"you do your best to decipher it. \n"
"It states, “Shorter than a Sword, Sharper than a Knife, I leave your enemies low, and take away their life” . \n"
              
"15a:The room starts to shake and the podium starts to ascend. \n"
"As you watch it reach the ceiling it turns and descends back down. "
"But now to your surprise a radiating dagger is in the center. You immediately recognize the dagger from your notes;\n "
"it's Nashor’s Tooth, the weapon of legend thought to have been lost to the void. \n"
"You observe the dagger is the perfect shape for the metallic door.\n "
"You slowly push the blade of the dagger and twist, at first nothing however \n"
"It opens into a well lit massive cave covered in different foliages. \n"
"To your surprise you see your mother in the center of the cave, however your joy quickly turns to horror as you see a gigantic bull like beast standing over her. \n"
"She sees you and before she can say anything the beast roars and strikes your mother down knocking her unconscious. \n"


"16: You don't know what this beast is or what it's capable of but you do know that if you don't act now your mother will perish.\n"
              
"16a: Gripping Nashors Tooth you feel a surge of adrenaline fill your body, \n"
"you swiftly dart to the side of the beast and slash at its left leg. \n"
"As it turns and quickly reaches for you cut through its arm and straight to its eyes. \n"
"The beast cries out in pain after being blinded by you, \n"
"however the dagger thirsts for more before you can think you drive the dagger through the chest of the beast.\n"

"16b: The beast raises both of its massive arms above its head to smash your mother. \n"
"Unaware of the beast's power, but knowing you must save your mother, you quickly act. \n"
"You slide between the beast's legs and grab your mother pulling her out of the way of the huge slam. \n"
"You see a small gap between the rocks, big enough for a person to make it through however too small for the beast. \n"
"Instinctively, you dart for the gap pulling your unconscious mother behind you.\n "
"As you slip through you feel the slamming of the beasts arms as it tries to force itself through the gap. \n" 
         
"16c: You feel anger and adrenaline fill your body, but before you can act you see a large statue, of what you could assume was Nasus,\n "
"along the wall of the cave. As much as you try to focus your eyes keep getting drawn to it. \n"
"The statue is perfectly intact, however it has an empty slot at the head of the statue.\n"
" You race towards the statue with the gem in hand,\n "
"as you reach you you glare to see the beast's massive arms raised high above his head ready to come crashing down upon your mothers body.\n "
"You shove the gem into the open slot of the statue and brace yourself to hear the loud impact of the beast's arms. \n"
"Instead it was all silence.\n"

"17: End"
              
"17a: After striking down the beast you quickly grab your mother and check on her. \n"
"She slowly opens her eyes and smiles looking at you. “Where's your father?” she asks. \n"
"You stare at her puzzled and tell her you haven't seen him. “He was right next to me!” she says frightened.\n "
"You both turn horrified to look at the beast's corpse as it begins shriveling and shaking only to reveal that it had been your father all along.\n"

"17b: The gap in the wall led to an open tunnel,\n"
" as you approached you gladly found that the tunnel had led you right outside to an oasis in the desert. \n"
"With the sun on you and your mothers faces she began to slowly awaken.\n"
" She looked at you happily however it quickly turned to confusion. “How did we get out and where is your father?” she asks.\n "
"You point to the small tunnel but before you can explain the ground begins to rumble and the tunnel collapses on itself.\n"
" You think nothing of it until you see the look of terror on your mothers face. \n"
"“Your Father was still in there, he was cursed, we needed to save him.”\n"        
              
"17c: You turned expecting to see the horrifying beast,\n"
"but it was gone and to your surprise instead your father stood over your mother holding her in his arms. \n"
"“You did good, I always knew you were better than the rest of us” he said walking over to you. \n"
"Finally being reunited with your family you were ready to get out of this cursed tomb, and your family happily agreed.\n"

"18\n"

              )

outfile.close()

