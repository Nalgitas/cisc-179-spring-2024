import random

###the object of the game is the identify this ghost
class Ghost:
    ghosts = ["hantu","obake","poltergheist","demon","mimic"]
    tell = ["you shiver as if a cold wind blue through you",
            "you hear wolves howling",
            "you tremble"]
    def __init__(self):
        self.type = self.ghosts[int(random.randrange(0,5))]
        self.evidence = ""
        self.room = room_list[list(room_list.keys())[int(random.randrange(1,len(room_list.keys())))]].name
        self.get_evi()

    def has_evi(self, test):
        ret = False
        if test == "all":
            return True
        for x in self.evidence:
            if x == test:
                ret = True
        return ret
    
    def __str__(self) -> str:
        return self.type + "    " + self.evidence

    def reveal(self):
        print("I am a " + self.type.upper() + " and you can tell me apart because of the " + self.evidence[0].upper() + " in the " + self.room)

    def get_evi(self):
        if self.type == "hantu":
            self.evidence = ["freezing"]
        if self.type == "demon":
            self.evidence = ["emf"]
        if self.type == "poltergheist":
            self.evidence = ["examine"]
        if self.type == "mimic":
            self.evidence = ["freezing","emf","examine","fingerprints"]
        if self.type == "obake":
            self.evidence = ["fingerprints"]

###you can find the ghost my searching through rooms of the house
class Rooms:
    def __init__(self, name, neighboors):
        self.name = name
        self.neighbors = neighboors
        self.description = ""

    def __eq__(self, other):
        return self.name == other.name
    
    def inside(self):
        return self.name == current_room
    
    def __str__(self):
        return self.description
    
    def addneighbor(self,name):
        self.neighbors.append(name)
    
    def changeDescription(self,desc):
        self.description = desc

    ###removes the possibility of teleporting between rooms
    def is_valid_move(self,move):
        return move in self.neighbors
    
#END OF CLASS DEFINITIONS

###uncomment "room_list" and move_room?

### LOAD ROOMS and GUIDES
room_list = {}
### Map
###            Bedroom -- Bathroom
###               |
###    Door -- Hallway -- Living Room -- Basement
###                          |
###              Garage --Kitchen -- Dining Room

roomsLoaded,described,guided,journaled,start_txt_loaded = False,False,False,False,False
guide,journal_notes,start_txt,ghostly_sign = [],[],"",[]
def indentifyType(lst):
    global roomsLoaded,described,guided,journaled,start_txt_loaded,guide,journal_notes,start_txt,ghostly_sign
    if not roomsLoaded:
        roomsLoaded = True
        assignRooms(lst)
    elif not described:
        assignRoomDescription(lst[0])
    elif not guided:
        for x in lst:
            guide.append(x)
        guided = True
    elif not journaled:
        for x in lst:
            journal_notes.append(x)
        journaled = True
    elif not start_txt_loaded:
        start_txt = lst[0]
        start_txt_loaded = True
    else:
        for x in lst:
            ghostly_sign.append(x)


        

def assignRoomDescription(desc):
    global room_list,described
    n = desc[:desc.index("-")]
    d = desc[desc.index("-")+1:]
    room_list[n].changeDescription(d)
    if n == "basement":
        described = True

def assignRoomLines(lines):
    global room_list
    room_list[lines[0]] = lines[1]

def assignRooms(lst):
    global room_list
    for x in lst:
        x = x.strip().lower()
        roomname = x[:x.index("-")].replace(" ","")
        room_list[roomname] = Rooms(roomname,x[x.index("-")+1:].split(","))

def readFromTXT():
    with open("huang.txt","r") as f:
        text = f.readlines()
        chunk = []
        for line in text:
            if "END_" in line:
                indentifyType(chunk)
                chunk = []
            else:
                chunk.append(line)



readFromTXT()
assert roomsLoaded and described and guided and journaled and start_txt_loaded







### initialize ghost room
spooky = Ghost()
# spooky.reveal() #TEST
current_room = "start"


###next essentially is the current input for processing
next = ""

###the moveto and d_ and guess are meant to help me separate different types of commands while parsing the command string
###the next few functions are just for displaying help related messages to teach the player the commands and game details
def help_menu():
    global next,guide
    display = ""
    for x in guide:
        display += x + "\n"
    next = input(display)
    next = ""
    room()

###teaches players about the ghosts
def journal():
    global next
    display = ""
    for x in journal_notes:
        display += x + "\n"
    next = input(display)
    next = ""
    room()

##handles movement commands
def change_rooms(move):
    global next,current_room,room_list
    if move == "door":
        next = input("You shake the handle but the door is firmly locked.")
        return detect_cmd()
    if room_list[current_room].is_valid_move(move):
        current_room = move
        return room()
    next = input("Invalid Move please try again or type d_Lost to get a description of the room\n")
    return detect_cmd()
    
###handles detection commands (evidences)
def explore():
    global current_room
    global spooky
    global next

    if next.find("lost") != -1:
        return room()
    
    if spooky.room.lower() != current_room:
        next = input("You look around but find nothing. What will you do next?\n")
        return detect_cmd()
    
    for x in spooky.evidence:
        if x == next:
            if next == 'freezing':
                display = "Brrrrr you found freezing temperatures!"
            if next == 'fingerprints':
                display = "You found fingerprints in this room."
            if next == 'emf':
                display = 'The EMF beeps loudly and reads at lvl 5.'
            if next == 'examine':
                display = 'The room is a mess! Objects seem to have been tossed around.'
            next = input(display + " What will you do next?\n")
            return detect_cmd()
    
    next = input("You look around but find nothing. What will you do next?\n")
    return detect_cmd()
    
### handles commands regarding the journal and a ghost guess
def ghost_help():
    global next
    if (next.find("notebook") != -1) or (next.find("journal") != -1):
        return journal()
    elif next.find("guess") != -1:
        next = next[5:]
        if next == spooky.type.lower():
            print("Good work hunter! Thanks for playing!")
        else:
            print("Incorrect. The ghost knows you tried to get rid of it. Everything is dark. You run for the door but the hallway keeps getting longer. The ghost catches you.\n Better luck next time. Thanks for playing!")
        quit()
    next = input("invalid command please try again or type help\n")
    return detect_cmd()
            

### whenever the user types and input this disects them into each of the primary types of commands which are categorized in the help cmd
def detect_cmd():
    ### to standardize all the inputs
    global next
    next = next.replace(" ","").lower()
    if current_room == 'start':
        return room()

    if next == "help":
        return help_menu()
    if next == "quit":
        return exit()
    
    if next.find("moveto") != -1:
        return change_rooms(next[6:])
    
    if next.find("d_") != -1:
        next = next[2:]
        return explore()

    if next.find("guess") != -1:
        return ghost_help()

    next = input("invalid command please try again or type help\n")
    return detect_cmd()



###Game Loop
def room():
    global next,current_room,spooky,room_list,start_txt,ghostly_sign
    if current_room == "start":
        while not (next == "ready" or next == "help"):
            next = input("Hello! Welcome to PhasNOphobia. Type 'Help' for help otherwise\n type 'Ready' to begin\n").lower().strip()
        next = input(start_txt).lower()
        current_room = "hallway"   

    #helps the player find the ghost
    if current_room == spooky.room:
        print("\n"+ghostly_sign[random.randrange(0,5)])


    next = input(str(room_list[current_room]))
    ###rooms in the house
#     old code
#if current_room == "hallway":
#         next = input("""You find yourself in a long decrepit hallway which splits off in two paths. 
# On your left you can see a bedroom, and on the right you see a living room. Which way will you go?\n""")

    return detect_cmd()
    


###starts the game loop
room()